# -*- coding: utf-8 -*-

from flask import Flask, json, render_template, flash, redirect, url_for, request
from flask.ext.pymongo import PyMongo
from icalendar import Calendar, Event
from datetime import date, datetime as dt
from lxml import etree as et
from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import DataRequired

# Essentially globalally defining the Flask app and Database
app = Flask(__name__)
app.config.from_object('settings')
app.config.from_object('secret')
mongo = PyMongo(app)


class User(object):
  def __init__(self, user, new=False):
    self.name = user
    self.movies = None
    self.session = None
    self.col = mongo.db.users
    if not new:
      details = self.col.find_one({'username': self.name})
      if details is not None:
        self.movies = details['movies']
      if details is not None:
        self.session = details['session']

  def save(self, movielist):
    if self.col.find_one({'username': self.name}):
      self.col.update({'username': self.name}, {'$set': {'movies': movielist}})
    else:
      details = {'username': self.name,
                 'session': self.session,
                 'movies': self.movies
                 }
      self.col.insert(details)


class Watchlist(object):
  def __init__(self, user):
    self.user = user
    self.col = mongo.db.movielist
    self.movies = self.col.find_one({'username': user})['movies']

  def date(self, block):
    """Takes a string in "YYYY-MM-DD" format and returns a python DATE object
    """
    a, b, c = map(int, block.split('-'))
    return date(a, b, c)

  def ical(self):
    cal = Calendar()
    cal.add('prodid', '-//Bt.tl Watchlist//TMDB Watchlist Schedule//EN')
    cal.add('version', '2.0')

    for item in self.movies:
      movie = self.col.find_one({'_id': item})
      event = Event()
      event.add('summary', movie['title'])
      event.add('description', movie['overview'])
      event.add('transp', 'TRANSPARENT')
      event.add('dtstart', self.date(movie['release_date']))
      event.add('uid', ''.join([str(movie['_id']), self.user]))
      cal.add_component(event)

    return cal.to_ical()

  def rss(self):
    now = dt.now()
    today = date(now.year, now.month, now.day)
    ulist = self.col.find({'_id': {'$in': self.movies}}).sort('release_date')
    tree = et.Element('rss', version='2.0')
    channel = et.SubElement(tree, 'channel')
    et.SubElement(channel, 'title').text = self.user + "'s Watchlist"
    et.SubElement(channel, 'link').text = "http://bt.tl/watchlist"
    et.SubElement(channel, 'description').text = "Feed of " + self.user + "\
's Watchlist brought to you by Trae Blain.  http://traebla.in/ http://bt.tl/"
    et.SubElement(channel, 'language').text = "en-us"
    et.SubElement(channel, 'pubDate').text = now.strftime(
      "%A, %B %d %Y %H:%M:%S CST"
    )
    et.SubElement(channel, 'lastBuildDate').text = now.strftime(
      "%A, %B %d %Y %H:%M:%S CST"
    )
    et.SubElement(channel, 'docs').text = "http://bt.tl/watchlist"
    et.SubElement(channel, 'managingEditor').text = "trae@traeblain.com"
    et.SubElement(channel, 'webMaster').text = "trae@traeblain.com"

    for movie in ulist:
      if self.date(movie['release_date']) > today:
        continue
      subItem = et.SubElement(channel, 'item')
      et.SubElement(subItem, 'title').text = movie['title']
      et.SubElement(subItem, 'link').text = 'http://www.themoviedb.org/\
movie/{0}'.format(str(movie['_id']))
      if len(movie['genres']) > 0:
        et.SubElement(subItem, 'category').text = movie['genres'][0]['name']
      if movie['poster_path'] is None:
        poster = "/posternotfound.jpg"
      else:
        poster = movie['poster_path']
      et.SubElement(subItem,
                    'description').text = et.CDATA(''.join([
                                       '<img src="', poster,
                                       '" style="float:left"><h2>',
                                       movie['title'], '</h2><h3>',
                                       movie['tagline'], '</h3><p>',
                                       movie['overview'],
                                       '</p><p>Brought to you by <a href="\
https://bt.tl/">Bottled Tools</a></p>']))
      et.SubElement(subItem, 'pubDate').text = movie['release_date']
      et.SubElement(subItem, 'guid').text = str(movie['_id']) + self.user

    return et.tostring(tree, pretty_print=True)

  def json(self):
    flist = self.col.find({'_id': {'$in': self.movies}}).sort('release_date')
    userlist = {'username': self.user, 'movies': flist}
    return json.dumps(userlist, indent=4, encoding="utf-8")


class SubmitUser(Form):
  name = TextField("username", validators=[DataRequired()])


@app.route('/', methods=("GET", "POST"))
def home():
  form = SubmitUser()
  if request.method == 'GET':
    userdetails = None
  if request.method == 'POST':
    print "Getting User: " + form.name.data
    userdetails = User(form.name.data)
    print "User Details: " + userdetails.name
  if form.validate_on_submit():
    flash("Success")
    return render_template("home.html", form=form, user=userdetails)
  return render_template("home.html", form=form, user=userdetails)

if __name__ == '__main__':
  app.run(host=app.config['HOST'], port=app.config['PORT'])
