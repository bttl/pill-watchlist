import requests
import copy
import json
from pymongo import MongoClient
from icalendar import Calendar, Event
from datetime import date, datetime as dt, timedelta as td
from lxml import etree

db = MongoClient().watchlist
movies = db.movielist
users = db.users


def easy_date(block):
  """ Takes a date in "YYYY-MM-DD" format and returns a python DATE object"""
  a, b, c = map(int, block.split('-'))
  return date(a, b, c)


def api(api_key, session=False):
  """ Returns the properly formated params structure for api calls. """
  if not session:
    return {"api_key": api_key}
  return {"api_key": api_key,
          "session_id": session,
          "page": 1}


def new_user(username, api):
  """Check if user exists in database, creates a users if not."""
  if users.find({'username': {'$in': [username]}}).count() == 0:
    tmdb_api = "http://api.themoviedb.org/3/authentication/token/new"
    user_request = requests.get(tmdb_api, params=api)
    if user_request.status_code != 200:
      return "ERROR"
    follow_url = user_request.headers['Authentication-Callback']
    return follow_url

  return follow_url


def reset_user(username):
  users.remove({'username': username})
  pass


def get_watchlist(userid, api):
  """Gets the user's watchlist from TheMovieDB and creates a list of items
  that are sorted by release date.
  """
  IDs = []
  tmdb_api = "http://api.themoviedb.org/3/account"
  get_url = '/'.join([tmdb_api, userid, 'movie_watchlist'])

  list_request = requests.get(get_url, params=api)
  list = list_request.json()['results']

  if list_request.json()['total_pages'] > 1:
    newapi = copy.deepcopy(api)
    newapi['page'] = 2
    while newapi['page'] <= list_request.json()['total_pages']:
      more_request = requests.get(get_url, params=newapi)
      list.extend(more_request.json()['results'])
      newapi['page'] = newapi['page'] + 1

  now = dt.now()
  twoweeks = date(now.year, now.month, now.day) - td(14)
  delIDs = []
  for item in list:
    if easy_date(item['release_date']) < twoweeks:
      delIDs.append(item['id'])
    item['_id'] = item['id']
    IDs.append(item['_id'])
    del(item['id'])

  newlist = [item for item in list if item['_id'] not in delIDs]
  newIDs = [item for item in IDs if item not in delIDs]

  return sorted(newlist, key=lambda item: item['release_date']), newIDs


def add_movie(movie_id, api):
  """Adds movie to the database or updates the details"""
  tmdb_api = "http://api.themoviedb.org/3/movie"
  get_url = '/'.join([tmdb_api, str(movie_id)])
  movie_request = requests.get(get_url, params=api)
  movie_data = movie_request.json()
  movie_data['_id'] = movie_data['id']
  del(movie_data['id'])

  movies.save(movie_data)

  return movie_data


def grab_details(list, api):
  """Populates a the list of movies in the watchlist with the additional
  details by either getting it from TheMovieDB or from the local database.
  """
  for item in list:
    if movies.find({'_id': {'$in': [item['_id']]}}).count() != 0:
      # print "{0} is in the database...".format(item['title'])
      db_movie = movies.find_one({'_id': item['_id']})
      for key in item:
        if key in ['popularity', 'vote_count', 'vote_average']:
          continue
        if item[key] != db_movie[key]:
          # print "{0}: {1} is different, updating databse.\
          #       ".format(item['title'], key)
          movie_data = add_movie(item['_id'], api)
          break
      if 'movie_data' in locals():
        item.update(movie_data)
        del(movie_data)
      else:
        item.update(movies.find_one({'_id': item['_id']}))
    else:
      # print "{0} is not in DB, adding.".format(item['title'])
      movie_data = add_movie(item['_id'], api)
      item.update(movie_data)

  pass


def update_user_movies(username, ids):
  """Updates the user's movielist with the data collected from
  get_watchlist()"""
  user_data = {'username': username, 'movies': ids}
  if users.find({'username': {'$in': [username]}}).count() == 0:
    users.insert(user_data)
  else:
    users.update({'username': username}, {'$set': {'movies': ids}})
  pass


def get_user_movies(username):
  """Returns the user's list of IDs from the database. If the user doesn't
  exist, this returns False"""
  if users.find({'username': {'$in': [username]}}).count() == 0:
    return False
  else:
    return users.find_one({'username': username})['movies']


def create_ical(username, list):
  cal = Calendar()
  cal.add('prodid', '-//Bt.tl Watchlist//TMDB Watchlist Schedule//EN')
  cal.add('version', '2.0')

  for item in list:
    movie = movies.find_one({'_id': item})
    event = Event()
    event.add('summary', movie['title'])
    event.add('description', movie['overview'])
    event.add('transp', 'TRANSPARENT')
    event.add('dtstart', easy_date(movie['release_date']))
    event.add('uid', ''.join([str(movie['_id']), username]))
    cal.add_component(event)

  return cal.to_ical()


def create_rss(username, list):
  now = dt.now()
  today = date(now.year, now.month, now.day)
  userlist = movies.find({'_id': {'$in': list}}).sort('release_date')
  tree = etree.Element('rss', version='2.0')
  channel = etree.SubElement(tree, 'channel')
  etree.SubElement(channel, 'title').text = username + "'s Watchlist"
  etree.SubElement(channel, 'link').text = "http://bt.tl/pills/#watchlist"
  etree.SubElement(channel, 'description').text = "Feed of " + username + "\
's Watchlist brought to you by Trae Blain.  http://traebla.in/ http://bt.tl/"
  etree.SubElement(channel, 'language').text = "en-us"
  etree.SubElement(channel, 'pubDate').text = now.strftime(
    "%A, %B %d %Y %H:%M:%S CST"
  )
  etree.SubElement(channel, 'lastBuildDate').text = now.strftime(
    "%A, %B %d %Y %H:%M:%S CST"
  )
  etree.SubElement(channel, 'docs').text = "http://bt.tl/pills/#watchlist"
  etree.SubElement(channel, 'managingEditor').text = "trae@traeblain.com"
  etree.SubElement(channel, 'webMaster').text = "trae@traeblain.com"

  for movie in userlist:
    if easy_date(movie['release_date']) > today:
      continue
    # print "Attempting to add {0} to the RSS Feed.".format(movie['title'])
    subItem = etree.SubElement(channel, 'item')
    etree.SubElement(subItem, 'title').text = movie['title']
    etree.SubElement(subItem, 'link').text = 'http://www.themoviedb.org/movie/\
{0}'.format(str(movie['_id']))
    if len(movie['genres']) > 0:
      etree.SubElement(subItem, 'category').text = movie['genres'][0]['name']
    if movie['poster_path'] is None:
      poster = "/posternotfound.jpg"
    else:
      poster = movie['poster_path']
    etree.SubElement(subItem,
                     'description').text = etree.CDATA(''.join([
                                          '<img src="',
                                          poster,
                                          '" style="float:left"><h2>',
                                          movie['title'],
                                          '</h2><h3>',
                                          movie['tagline'],
                                          '</h3><p>',
                                          movie['overview'],
                                          '</p><p>Brought to you by <a href="\
https://bt.tl/">Bottled Tools</a></p>']))
    etree.SubElement(subItem, 'pubDate').text = movie['release_date']
    etree.SubElement(subItem, 'guid').text = str(movie['_id']) + username

  return etree.tostring(tree, pretty_print=True)


def create_json(username, fullList):
  userlist = {'username': username, 'movies': fullList}
  return json.dumps(userlist, indent=4, encoding="utf-8")
